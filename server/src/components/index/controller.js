const index =  async (ctx, next) =>  {
    ctx.body = `API for account notebook`;
    await next();
}

module.exports = { index }