const Router = require('koa-router');
const KoaBody = require('koa-body');
const { fetchHistory, commitNewToAccount, findById } = require('./controller');

const router = new Router({
    prefix: '/transactions'
});

router
    .get('/',  fetchHistory)
    .get('/:id',    findById)
    .post('/',      KoaBody(), commitNewToAccount)

module.exports = router;