const moment = require('moment');
const { v4: uuidv4 } = require('uuid');

class Transactions {

    constructor() {
        this.types = ['debit', 'credit'];
        this.map = new Map();
        this.total = 0;
        this.readLock = false;
        this.writeLock = false;
    }

    getList() {
        if(this.readLock) {
            throw new Error('Read is locked');
        }
        const list = [];
        for(const [, value] of this.map) {
            list.push(value);
        }
        return { transactions: list, total: this.total };
    }

    clearLock() {
        this.writeLock = false;
        this.readLock = false;
    }

    lock() {
        this.writeLock = true;
        this.readLock = true;
    }

    addNew(type, amount ) {
        if(this.writeLock) {
            throw new Error('Write is locked');
        }
        this.lock();
        amount = parseFloat(amount);
        if(!this.types.includes(type)) {
            this.clearLock();
            throw new Error(`type: ${type} not allowed, use: ${this.types.join(',')}`);
        }
        if(!( amount > 0)) {
            this.clearLock();
            throw new Error(`amount: ${amount} invalid`);
        }
        const item = {
            id: uuidv4(),
            type,
            amount: amount,
            effectiveDate: moment().toISOString()
        };
        this[type](amount);
        this.map.set(item.id, item);
        this.clearLock();
        return item;
    }

    debit(amount) {
        this.total = this.total + amount
        return this.total
    }

    credit(amount) {
        if( (this.total - amount) < 0) {
            this.clearLock()
            throw new Error('Should not be negative total');
        }
        this.total = this.total - amount
        return this.total
    }

    get(id) {
        if(!this.map.has(id)) {
            throw new Error(`Key ${id} not found`);
        }
        return this.map.get(id);
    }

}

module.exports = Transactions;