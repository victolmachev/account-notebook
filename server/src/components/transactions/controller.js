const { Transactions } = require('./models');
const transactions = new Transactions();

const fetchHistory =  async (ctx, next) =>  {
    ctx.body = await transactions.getList();
    await next();
}

const commitNewToAccount =  async (ctx, next) =>  {
    const { type, amount } = ctx.request.body;
    try {
        ctx.body = await transactions.addNew(type, amount);
    } catch (e) {
        ctx.throw(400, { message: e.message })
    }
    await next();
}

const findById = async (ctx, next) => {
    const { id } = ctx.params;
    try {
        ctx.body = await transactions.get(id);
    } catch (e) {
        ctx.throw(404, { message: e.message })
    }
    await next();
}

module.exports = {
    fetchHistory,
    commitNewToAccount,
    findById
}