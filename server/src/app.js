const Koa = require('koa');
const logger = require('koa-logger');
const cors = require('@koa/cors');
const app = new Koa();
app.use(logger());
app.use(cors());

const indexRouter = require('./components/index/router')
const transactionsRouter = require('./components/transactions/router')

app.use(indexRouter.routes());
app.use(indexRouter.allowedMethods());

app.use(transactionsRouter.routes());
app.use(transactionsRouter.allowedMethods());

const server = app.listen(4000);
module.exports = server;