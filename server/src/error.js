
module.exports = async (ctx, next) => {
    try {
        await next();
    } catch (err) {
        ctx.status = err.statusCode || err.status || 500;
        // will only respond with JSON
        ctx.status = status;
        ctx.body = err.message;
        ctx.app.emit('error', err, ctx);
    }
};