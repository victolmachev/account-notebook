- Please run installation project using `yarn install`
- Then start full stack app using `yarn start`
- Please use to load in browser http://localhost:3000/
- Call to create *POST* `localhost:4000/transactions` `{"type": "credit", "amount": 6}`
- Then refresh the page