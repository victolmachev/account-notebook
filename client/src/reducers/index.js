import {
    ADD_TRANSACTION,
    ADD_TRANSACTION_DONE,
    FETCH_TRANSACTIONS,
    FETCH_TRANSACTIONS_DONE,
    CLEAR_STATE
} from "../types";

const initialState = {
    transactions: [],
    transaction: {},
    total: 0,
    loading: false
};

const indexReducer = (state = initialState, {type, payload}) => {
    console.log('reducer', type, payload);
    switch (type) {
        case ADD_TRANSACTION:
            return {...state, loading: payload.loading, transaction: payload.transaction };
        case ADD_TRANSACTION_DONE:
            return {...state, loading: payload.loading, transaction: payload.transaction };
        case FETCH_TRANSACTIONS:
            return {...state, loading: payload.loading };
        case FETCH_TRANSACTIONS_DONE:
            return {...state, loading: payload.loading, transactions: payload.transactions, total: payload.total };
        case CLEAR_STATE:
            return initialState;
        default:
            return state
    }
};

export default indexReducer;