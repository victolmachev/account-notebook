import {
    ADD_TRANSACTION,
    ADD_TRANSACTION_DONE,
    FETCH_TRANSACTIONS,
    FETCH_TRANSACTIONS_DONE
} from "../types";

export const addTransaction = ({loading = true, transaction}) => {
    return { type: ADD_TRANSACTION, payload: { loading, transaction } };
};

export const addTransactionDone = ({ loading = false, transaction }) => {
    return { type: ADD_TRANSACTION_DONE, payload: { loading, transaction }};
};

export const fetchTransactions = ({ loading = true }) => {
    return { type: FETCH_TRANSACTIONS, payload: { loading }};
};

export const fetchTransactionsDone = ({ loading = false, transactions, total }) => {
    return { type: FETCH_TRANSACTIONS_DONE, payload: { loading, transactions, total }};
};
