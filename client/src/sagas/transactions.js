import { put, takeEvery, call } from 'redux-saga/effects';
import { fetchTransactionsAPI, storeTransactionAPI } from '../api'
import { ADD_TRANSACTION, FETCH_TRANSACTIONS } from '../types'
import { addTransactionDone, fetchTransactionsDone } from "../actions";

function* workStoreTransaction(data) {
    try {
        const { transaction } = data.payload;
        const payload = yield call(storeTransactionAPI, { transaction });
        payload.loading = false;
        yield put(addTransactionDone(payload));
    } catch (e) {
        console.error(e);
    }
}

export function* watchStoreTransaction() {
    yield takeEvery(ADD_TRANSACTION, workStoreTransaction)
}

function* workFetchTransactions(data) {
    try {
        const payload = { ...yield call(fetchTransactionsAPI), loading: false };
        yield put(fetchTransactionsDone(payload));
    } catch (e) {
        console.error(e);
    }
}

export function* watchFetchTransactions() {
    yield takeEvery(FETCH_TRANSACTIONS, workFetchTransactions)
}