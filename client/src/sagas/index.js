import { all } from 'redux-saga/effects';
import { watchFetchTransactions, watchStoreTransaction } from './transactions'

export default function* rootSaga() {
    yield all([
        watchFetchTransactions(),
        watchStoreTransaction()
    ]);
}