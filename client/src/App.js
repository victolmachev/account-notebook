import './App.css';
import Transactions from './components/Transactions';

function App() {
  return (
    <div className="App">
        <h3>Account Notebook</h3>
        <div>
            <Transactions />
        </div>
    </div>
  );
}

export default App;
