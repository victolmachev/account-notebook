import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { useSelector, useDispatch } from 'react-redux'
import { fetchTransactions } from '../actions'

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
    credit: {
        color: "#fff",
        backgroundColor: "#ff4569"
    },
    debit: {
        color: "#000",
        backgroundColor: "#a2cf6e"
    }
}));

let initial = true;
const signs = {'debit': '+', 'credit': '-'};

export default function Transactions() {
    const classes = useStyles();
    const { transactions = [], total = 0 } = useSelector(state => {
        return { transactions: state.transactions, total: state.total }
    } );
    const dispatch = useDispatch()
    if(initial) {
        initial = false
        dispatch(fetchTransactions({}))
    }
    const transactionsList = transactions.map(transaction =>
            <Accordion className={classes[transaction.type]}>
                <AccordionSummary expandIcon={<ExpandMoreIcon />} >
                    <Typography className={classes.heading}>
                        Transaction {signs[transaction.type]} {transaction.amount}
                    </Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        <List>
                            <ListItem>
                                <ListItemText
                                    primary="Type"
                                    secondary={transaction.type}
                                />
                            </ListItem>
                            <ListItem>
                                <ListItemText
                                    primary="Amount"
                                    secondary={transaction.amount}
                                />
                            </ListItem>
                            <ListItem>
                                <ListItemText
                                    primary="Date"
                                    secondary={transaction.effectiveDate}
                                />
                            </ListItem>
                        </List>
                    </Typography>
                </AccordionDetails>
            </Accordion>
    )
    return (
        <div className={classes.root}>
            <h4>Total {total}</h4>
            {transactionsList}
        </div>
    );
}