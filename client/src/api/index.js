import { jsonFetch } from './lib';
import { API_URL } from '../config';

export const storeTransactionAPI = async({transaction}) => {
    return jsonFetch(`${API_URL}/transactions`, 'POST', { transaction });
};

export const fetchTransactionsAPI = async () => {
    return jsonFetch(`${API_URL}/transactions`, 'GET');
};