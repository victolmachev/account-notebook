const handle = async (url, method = 'GET', body = null, ...options)  =>{
    const headers = {
        'content-type': 'application/json',
    };
    const params = {
        method: method,
        headers,
        ...options
    };
    if(body) {
        params.body =  JSON.stringify(body);
    }

    const response = await fetch(url, params);
    console.log({ status: response.status});

    if([403, 401].includes(response.status)) {
        throw new Error(`Access denied ${response.status}`);
    }

    if (!response.ok) {
        throw new Error(`Fetch error: ${response.statusText}`);
    }

    return response

}

export const jsonFetch = async (url, method = 'GET', body = null, ...options) => {
    const response = await handle(url, method, body, options);
    return await response.json();
};